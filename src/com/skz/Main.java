package com.skz;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        VisibilityThread.test();
        UnsafeCounting.test();
        MyThreadPoolExecutor.test();
    }


}
