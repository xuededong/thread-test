package com.skz;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private List<String> storage;//生产者和消费者共享的仓库

    public Storage() {
        storage = new ArrayList<>();
    }

    public List<String> getStorage() {
        return storage;
    }

    public void setStorage(List<String> storage) {
        this.storage = storage;
    }

    public static void main(String[] args) {
        Storage storage = new Storage();
        Producer producer = new Producer(storage.getStorage());
        Consumer consumer = new Consumer(storage.getStorage());
        producer.start();
        consumer.start();
    }

}
