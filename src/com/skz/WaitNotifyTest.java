package com.skz;

public class WaitNotifyTest {
    private Object lock = new Object();

    public void wait1() throws InterruptedException {
        System.out.println("before wait");
        synchronized (lock){
            lock.wait();
            System.out.println("after wait");
        }
    }

    public void notify1(){
        System.out.println("before notify");
        synchronized (lock){
            lock.notify();
            System.out.println("after notify");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        WaitNotifyTest waitNotifyTest = new WaitNotifyTest();
        new Thread(() -> {
            try {
                waitNotifyTest.wait1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        Thread.sleep(100);
        waitNotifyTest.notify1();
    }
}
