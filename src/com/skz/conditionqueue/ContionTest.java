package com.skz.conditionqueue;

public class ContionTest {

    public void testSleep() throws InterruptedException {
        SleepyBoundedBuffer<String> buffer = new SleepyBoundedBuffer<>(5);
        new Thread(() -> {
            String value = buffer.take();
            System.out.println("获取到：" + value);
        }).start();

        Thread.sleep(2200);
        new Thread(() -> {
            buffer.put("123");

        }).start();
    }
}
