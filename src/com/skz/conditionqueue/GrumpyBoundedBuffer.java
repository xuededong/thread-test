package com.skz.conditionqueue;

public class GrumpyBoundedBuffer<V> extends BaseBoundedBuffer<V> {
    public GrumpyBoundedBuffer(int size) {
        super(size);
    }

    public synchronized void put(V v) {
        if (isFull()) {
            throw new RuntimeException("满了");
        }
        doPut(v);
    }

    public synchronized V take() {
        if (isEmpty()) {
            throw new RuntimeException("空了");
        }
        return doTake();
    }
}
