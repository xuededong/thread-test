package com.skz.conditionqueue;

public class SleepyBoundedBuffer<V> extends BaseBoundedBuffer<V> {

    public SleepyBoundedBuffer(int size) {
        super(size);
    }

    public void put(V v) {
        while (true) {
            System.out.println("执行put(),value=" + v);
            //使用同步代码块，一次判断完成后释放锁，如果满足条件跳出循环，不满足则轮询[休眠-继续判断]
            synchronized (this) {
                if (!isFull()) {
                    doPut(v);
                    return;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public V take()  {
        while (true) {
            System.out.println("执行take()");
            synchronized (this) {
                if (!isEmpty()) {
                    return doTake();
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}