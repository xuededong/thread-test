package com.skz.conditionqueue;

public abstract class BaseBoundedBuffer<E> {
    private final E[] buf;
    private int tail;
    private int head;
    private int count;
    protected BaseBoundedBuffer(int capacity) {
        this.buf = (E[]) new Object[capacity];
    }

    protected synchronized final void doPut(E E) {
        buf[tail] = E;
        if (++tail == buf.length) {
            tail = 0;
        }
        ++count;
    }

    protected synchronized final E doTake() {
        E E = buf[head];
        buf[head] = null;
        if (++head == buf.length) {
            head = 0;
        }
        --count;
        return E;
    }

    public synchronized final boolean isFull() {
        return count == buf.length;
    }

    public synchronized final boolean isEmpty() {
        return count == 0;
    }
}
