package com.skz.conditionqueue;

public class BoundedBuffer<V> extends BaseBoundedBuffer<V> {

    // 条件谓词：not-full (!isFull())
    // 条件谓词：not-empty (!isEmpty())
    public BoundedBuffer( int size) {
        super (size);
    }

    // 堵塞并直道：not-full
    public synchronized void put(V v) throws InterruptedException{
        while (isFull()){
            wait();
        }
        doPut(v);
        notifyAll();
    }

    // 堵塞并直道：not-empty
    public synchronized V take() throws InterruptedException{
        while (isEmpty()){
            wait();
        }
        V v = doTake();
        notifyAll();
        return v;
    }
}
