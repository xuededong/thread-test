package com.skz;

import java.util.concurrent.CountDownLatch;

/**
 * @author songkaizong
 */
public class CountDownTest {
    private CountDownLatch countDownLatch = new CountDownLatch(3);

    public static void main(String[] args) throws InterruptedException {
        CountDownTest countDownTest = new CountDownTest();
        countDownTest.test();
        System.out.println("-----验证不会再改变状态，这扇门将永远保持打开-------");
        countDownTest.test();
    }

    public void test() throws InterruptedException {
        new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("A搞到粉条回来了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }).start();
        new Thread(() -> {
            try {
                Thread.sleep(2000);
                System.out.println("B搞到白菜回来了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }).start();
        new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("C搞到猪肉回来了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }).start();
        System.out.println("我先开始烧火了。");
        countDownLatch.await();
        System.out.println("材料都到齐了，开始做。");
    }
}
