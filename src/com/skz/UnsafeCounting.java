package com.skz;

public class UnsafeCounting {
    public int count;

    public void add() {
        for (int i = 0; i < 10000; i++) {
            count++;
        }
    }
    public static void test() throws InterruptedException {
        UnsafeCounting unsafeCounting = new UnsafeCounting();
        Thread t1 = new Thread(() -> unsafeCounting.add());
        Thread t2 = new Thread(() -> unsafeCounting.add());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(unsafeCounting.count);
    }
}
